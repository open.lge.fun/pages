module.exports = {
    title: '假性LG论坛',
    description: '由GitlabPages搭建的文档站',
    base: '/',
    dest: 'public',
  themeConfig: {
    displayAllHeaders: true ,
    repo: 'https://gitlab.com/open.lge.fun/pages/',
    repoLabel: 'Gitlab',
    docsDir: 'docs',
    editLinks: true,
    editLinkText: '在Gitlab编辑这个页面' ,
    sidebar: 'auto',
  }
}
