---
title: LG G7 解BOOTLOADER锁教程
---
**请确保你看完每一个字再下手！**
# LG G7 解BOOTLOADER锁教程
[[toc]]
> * 感谢telegram@j0sh1x 发现并分享的BUG。 <br /> 
> * 感谢俄罗斯论坛提供的V35工程ABL分区。 <br /> 
> * 感谢如如的技术指导。 <br /> 
> * 感谢LG工程师埋下的BUG。  <br /> 
> * 本教程由黑字编写，在未征得本人同意的情况下不得转发。

这是一个非常低级的错误，有了V20和V30在前面铺垫让我总感觉是LG系统开发人员的故意为之。[LineageOS的作者·LG G7唯一的第三方系统开发者·G7的TWRP开发者·Aljoshua Hell](https://t.me/J0SH1X)发现[G710ULM11g](https://1.lge.fun/guide/kdz.md)固件可以用`fastboot boot filename.img`这条指令，于是尝试着boot了一个rooted boot后成功开机了。实际上ULM11e也可以boot起来，貌似ulm的任意固件都可以boot起来。我们在测试韩版和别的版本

::: tip 提示
root过了或者bootloader解过了滚出教程 别继续看了
:::

::: danger 版本不通
V35的工程版ABL分区适用于OREO，并不适用于PIE！9.0请降级！
:::

::: warning 版本不通
目前已测试通过设备为少数版本，其他版本也许在某一步失败，请不要悲伤。
:::

::: warning 韩版提示
韩版刷完之后是进不去，别的boot刷完了只会变红，如果有什么好方法可以即使联系黑字测试。
:::

::: danger 良心运营商警告
T版刷别的kdz会9008，速度离开本教程。
:::

::: tip 成功版本
V版可以刷成ULM解bl <br />
USC版可以直接在vmx固件上面刷 <br />
EAW可直接在EAW本固件上刷入 <br />

:::

## 视频教程
<iframe style="width:100%;" src="//player.bilibili.com/player.html?aid=61816954&cid=107489262&page=1" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"> </iframe>
[https://www.bilibili.com/video/av61816954](https://www.bilibili.com/video/av61816954)
## W.预先准备
::: danger 清空警告
在执行`fastboot oem unlock`成功时会清空数据，请备份重要文件后再上车。
:::
* 正常工作的非XP电脑
* ADB工具包
* ULM11g系统的G7
* ULM11g的Rooted Boot
* 可用于解锁的abl分区映像

[部分文件下载地址](https://share.weiyun.com/5Tr3gQ6)   咒语~~XDZDLJ~~

::: tip
 以下教程可能对于你来说过分烧脑，**任意地方看不懂请联系黑字远程**，30块钱一次。~~看不懂就再看一遍，如果不想花钱的话。~~
:::

> 黑字企鹅号：2436526604

:::  danger 责任声明
以下的操作可能会**损坏你的设备**，当你进入[W.预先准备](#W.预先准备)步骤时默认你已经知道了这一条提示，刷机过程中出现任意问题与我无关，**请谨慎做出选择**。<br />
:::

-----

## A.获取ROOT权限
首先在手机里把连接方式改为`正在/仅充电`然后再开发者选项内打开`USB调试`和·`允许OEM解锁`

解压ADB工具包和abl.zip，把Rooted boot重命名为`rootedboot.img`，并移动到和adb工具包里面的`adb.exe`同目录下，解压出来的`abl_a`也复制进去，现在的目录如下(./代表任意目录)
```
./adb/adb.exe
./adb/……
./adb/fastboot.exe
./adb/rootedboot.img
./adb/abl_a
```
在`adb.exe`目录下的空白处点击`Shift+右键`选择`在此处打开PowerShell/执行窗口`。执行下面的指令（::后面的可以不复制）

```bash
cmd
adb reboot bootloader ::重启后继续
fastboot devices
fastboot boot rootedboot.img
fastboot reboot ::重启后继续
```
如果一切正常的话就会有下面的结果。
```
> cmd
Microsoft Windows [版本 *************]
(c) 2018 Microsoft Corporation。保留所有权利。

>adb devices
List of devices attached
* daemon not running; starting now at tcp:5037
* daemon started successfully
LMG710***********      device

>adb reboot bootloader

>fastboot devices
LMG710***********       fastboot

>fastboot boot rootedboot.img
Downloading 'boot.img'                             OKAY [  0.506s]
booting                                            OKAY [  0.052s]
Finished. Total time: 0.639s
```
窗口别关，按道理来说你已经重启了，重启完成多出了个`MagiskManager`。

* 如果卡在了`< wait for any device >`麻烦去翻译一下什么意思谢谢。
* 如果`fastboot boot rootedboot.img`卡在了`downloading boot.img`，去设备管理器检查一下`Android Devices`或者`Kedacom Devices`的设备，如果是`Android Sooner Single ADB Interface`那就更新驱动成为本地驱动的`Android Bootloader Interface`，没有详细教程。

## B.解BOOTLOADER锁
打开MagiskManager配置一波，等可以正常使用时执行下面的指令。
```bash
adb devices
adb push abl_a /sdcard/
adb shell " su -c 'dd if=/sdcard/abl_a of=/dev/block/bootdevice/by-name/abl_a'"
adb reboot bootloader ::重启后继续
fastboot oem unlock
fastboot flash boot_a rootedboot.img && fastboot flash boot_b rootedboot.img && fastboot reboot ::此处留意手机是否有黄色的警告画面
```
如果一切正常的话就会有下面的结果。
```
>adb devices
List of devices attached
LMG710***********      device

>adb push abl_a /sdcard/
D:\any\file\abl_a  14.6 MB/s (1048576 bytes in 0.068s)

>adb shell " su -c 'dd if=/sdcard/abl_a of=/dev/block/bootdevice/by-name/abl_a '"
2048+0 records in
2048+0 records out
1048576 bytes transferred in 0.094 secs (11155063 bytes/sec)

>adb reboot bootloader

>fastboot oem unlock
(bootloader) Erasing userdata and cache
OKAY [  4.073s]
Finished. Total time: 4.075s

>fastboot reboot
Rebooting
Finished. Total time: 0.002s


```
有时候在刷固件的时候机型会出错如：`LG-V350ULM`或者`Unknown,11G`之类的
```shell
adb shell "su-c'echo Rooted'" ::给Shell授权Root权限
adb shell " su -c'dd if=/dev/block/bootdevice/by-name/abl_b of=/dev/block/bootdevice/by-name/abl_a'"  ::还原B分区的abl到A分区
```
```
>adb shell "su-c'echo Rooted'" 
Rooted
>adb shell " su -c'dd if=/dev/block/bootdevice/by-name/abl_b of=/dev/block/bootdevice/by-name/abl_a'" 
2048+0 records in
2048+0 records out
1048576 bytes transferred in 0.094 secs (11155063 bytes/sec)
```
## C.打开Pornhub看小蝶热舞冲一发
庆祝一下你等到了解bootloader，等等党胜利！

--------
###### PoweredBy-HEIZI 2019.07.25
![](https://1.lge.fun/P/heizi.png)