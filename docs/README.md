---
heroImage: /hero.png
home: true
actionText: 立即创建一个页面！
actionLink: /heizi/mkpages
features:
- title: OPEN 
  details: 由GitlabPages和VuePress驱动，不必担心任何问题。
- title: LESS
  details: 以 Markdown 为中心的项目结构，只需要简单的几个字符即可创造复杂精美的页面。
- title: FREE 
  details: 绝对不收取任意费用。

footer: 由OPEN.LGE.FUN维护组制作。
---
#### 创建一个页面就像往头上泼矿泉水一样简单
你只需要新建一个以`.md`结尾的文件，然后开始行云流水的编写。
```markdown
# 第一步 准备
1. 买一瓶`矿泉水`
2. 在微博说出自己的计划

## 第二步 浇水
大方从容得走到CEO的面前扭开盖子、举起手臂、反转水瓶，淡定的下台迎接第二天的舆论和律师函。
```
当上传并且页面生成作业完成时，打开`open.lge.fun/新建的md文件文件名`就会和下面的内容一样。

----------
# 第一步 准备
1. 买一瓶`矿泉水`
2. 在微博说出自己的计划

## 第二步 浇水
大方从容得走到CEO的面前扭开盖子、举起手臂、反转水瓶，淡定的下台迎接第二天的舆论和律师函。